import { useState } from "react";
import { Component } from "./Component";

// Uncomment this below to prove import work or not
import { add } from "@repo/math-helpers";

export function App() {
  const [count, setCount] = useState(0);
  return (
    <>
      <Component />
      {count} Welcome <button onClick={() => setCount(count + 1)}>Add</button>
      {add(1, 2)}
    </>
  );
}

export default App;
