import path from "path";
import HtmlWebpackPlugin from "html-webpack-plugin";
import CopyWebpackPlugin from "copy-webpack-plugin";
import ReactRefreshWebpackPlugin from "@pmmmwh/react-refresh-webpack-plugin";
import ReactRefreshTypeScript from "react-refresh-typescript";
import { Configuration as WebpackDevServerConfiguration } from "webpack-dev-server";
import { Configuration as WebpackConfiguration } from "webpack";

interface Configuration extends WebpackConfiguration {
  devServer?: WebpackDevServerConfiguration;
}

type Environment = "production" | "development" | undefined;

const isDevelopment = process.env.NODE_ENV !== "production";

const htmlWebpackPlugin = new HtmlWebpackPlugin({
  template: "./public/index.html",
  filename: "index.html",
});

const copyWebpackPlugin = new CopyWebpackPlugin({
  patterns: [
    {
      from: "./public",
      to: "",
      globOptions: {
        ignore: [path.join(__dirname, "public", "index.html")],
      },
    },
  ],
});

const config: Configuration = {
  mode: (process.env.NODE_ENV as Environment) ?? "development",
  devServer: {
    liveReload: false,
    hot: true,
    static: {
      directory: path.join(__dirname, "dist"),
      publicPath: "./public",
    },
    port: 3001,
    historyApiFallback: {
      index: "/public/index.html",
    },
  },
  module: {
    rules: [
      {
        test: /\.(png|svg|jpg|jpeg|gif)$/i,
        type: "asset/resource",
      },
      {
        test: /\.(ts|tsx)$/,
        use: "ts-loader",
      },
      {
        test: /\.css$/,
        use: ["style-loader", "css-loader"],
      },
      {
        test: /\.[ts|tsx]?$/,
        use: [
          {
            loader: require.resolve("ts-loader"),
            options: {
              getCustomTransformers: () => ({
                before: [isDevelopment && ReactRefreshTypeScript()].filter(
                  Boolean
                ),
              }),
              transpileOnly: isDevelopment,
            },
          },
        ],
      },
    ],
  },
  resolve: {
    extensions: [".ts", ".tsx", ".js", ".jsx"],
    symlinks: false,
  },
  plugins: [
    isDevelopment && new ReactRefreshWebpackPlugin(),
    copyWebpackPlugin,
    htmlWebpackPlugin,
  ].filter(Boolean),
};

export default config;
