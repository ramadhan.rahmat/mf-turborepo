/** @type {import('next').NextConfig} */
module.exports = {
  transpilePackages: ["@repo/ui", "math-helpers"],
};
